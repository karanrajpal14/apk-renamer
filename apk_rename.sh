#!/bin/bash

rename() {
	local manifest=$(tools/./aapt debug badging "$1")
	local label=$(echo $manifest | grep -Po "(?<=application: label=')(.+?)(?=')")
	local package_name=$(echo $manifest | grep -Po "(?<=package: name=')(.+?)(?=')")
	local version_code=$(echo $manifest | grep -Po "(?<=versionCode=')(.+?)(?=')")
	local version_name=$(echo $manifest | grep -Po "(?<=versionName=')(.+?)(?=')")
	local apk_name="$label - $version_name - $version_code - $package_name.apk"

	if [ "$apk_name" == " -  -  - .apk" ];	then
		echo "🚫  $1 : faild to rename this file"
		return
	fi

	echo
	mv -f "$1" "renamed/$apk_name"
	echo "✅ Old name: $1 👇"
	echo "✅ New name: $apk_name"
	echo
	echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
	echo
}

for apk in input/*.apk
do
	rename "$apk"
done
